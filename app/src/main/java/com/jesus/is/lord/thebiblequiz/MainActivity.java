package com.jesus.is.lord.thebiblequiz;


import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Locale;

/**
 * This app presents the user with various questions and allows either a single response from
 * multiple choices, multiple responses from multiple choices, or a text response type by the user.
 * The questions, choices, and answers come from the strings.xml file and map to views, according to
 * type, in the activity_main.xml. To add more questions, one has to modify these two files and the
 * integers.xml file. The code will adapt to as many questions as memory will allow.
 */
public class MainActivity extends AppCompatActivity {
    private static Resources QUIZ_RESOURCES;//hold reference created in onCreate below
    private static String PACKAGE_NAME;//assign value in onCreate below
    private static int NUM_QUESTIONS, TOAST_DURATION;//assign values in onCreate below
    public enum QuestionType {single, multiple, text}//use to determine scoring method
    //used to inform user how they did on each question so they can correct mistakes and score again
    public enum AnswerStatus {unanswered, correct, incorrect}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //get reference to package resources from app\res\layout\*.xml and app\res\values\*.xml
        PACKAGE_NAME = getApplicationContext().getPackageName();
        QUIZ_RESOURCES = getBaseContext().getResources();
        int resId = QUIZ_RESOURCES.getIdentifier("number_of_questions", "integer",
                PACKAGE_NAME);
        NUM_QUESTIONS = QUIZ_RESOURCES.getInteger(resId);//retrieve from integers.xml file
        TOAST_DURATION = Toast.LENGTH_LONG;
        View all = findViewById(R.id.global_view);
        ViewTreeObserver vto = all.getViewTreeObserver();
        vto.addOnGlobalFocusChangeListener(new ViewTreeObserver.OnGlobalFocusChangeListener() {
            /**
             * Called after user chooses Next action on soft keyboard to hide keyboard
             * if newFocus is not an EditText view. Calls performClick if newFocus is score button
             * @param oldFocus previous view that had focus
             * @param newFocus new view with focus
             */
            public void onGlobalFocusChanged(
                    View oldFocus, View newFocus) {
                //without first if case, the user had to touch score button twice
                if (newFocus != null && (newFocus instanceof Button)) {
                    Button tempButton = (Button) newFocus;
                    tempButton.performClick();//produces sound and calls scoreQuiz method
                } else if (newFocus != null && !(newFocus instanceof EditText)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(newFocus.getWindowToken(), 0);
                }
            }
        });
    }


    /**
     * This method is called when user presses the score button. It compares the users answers to
     * those values found in the res\values\strings.xml file and adds a status of "UNANSWERED",
     * "correct", or "incorrect" to the questionResults string. The user is presented with a toast
     * with header RESULTS, score = #correct out of #NUM_QUESTIONS, and a numbered list of question
     * status. It allows for as many questions as exist in that file as long as the
     * activity_main.xml file is updated with a corresponding view to support the question type.
     *
     * @param view unused.
     */
    public void scoreQuiz(View view) {
        int resId;
        int score = 0;
        //use to accumulate questions status and display with score
        StringBuilder questionResults = new StringBuilder();

        //loop through each question starting at "question_1" and score it
        for (int questionIndex = 1; questionIndex <= NUM_QUESTIONS; questionIndex++) {
            //get resource id of question_#_type from strings.xml
            //force to use english since this is hardcoded in the strings.xml resource file
            resId = QUIZ_RESOURCES.getIdentifier(String.format(Locale.US, "question_%d_type",
                    questionIndex), "string", PACKAGE_NAME);
            //convert resource value to enum using resID.  Trim beginning and ending whitespace.
            QuestionType qType = QuestionType.valueOf(QUIZ_RESOURCES.getString(resId).trim().toLowerCase());
            //score each question based on qType
            switch (qType) {
                case single:
                    //get resource value using questionIndex and trim leading and trailing whitespace
                    String correctAnswer = QUIZ_RESOURCES.getString(getSingleAnswerId(questionIndex)).trim();
                    //get the RadioGroup view using the questionIndex
                    RadioGroup radioButtons = findViewById(getGroupViewId(questionIndex));
                    //get the selected button and text. returns -1 if no radio button is selected
                    int selectedId = radioButtons.getCheckedRadioButtonId();
                    if (selectedId == -1) {
                        score += updateResults(questionResults, AnswerStatus.unanswered, questionIndex);
                    } else {
                        RadioButton radioChecked = radioButtons.findViewById(selectedId);
                        if (radioChecked.getText().toString().equalsIgnoreCase(correctAnswer)) {
                            score += updateResults(questionResults, AnswerStatus.correct, questionIndex);
                        } else {
                            score += updateResults(questionResults, AnswerStatus.incorrect, questionIndex);
                        }
                    }
                    break;
                case multiple:
                    boolean ansIsCorrect = true;//stays true unless a checkbox has wrong boolean state
                    boolean questionWasAnswered = false;//stay false unless at least one box is checked
                    boolean choiceIsChecked;//store state of a single checkbox
                    //get resource values for the answers to question based on questionIndex
                    String[] questAnswers = QUIZ_RESOURCES.getStringArray(getQuestionAnswersId(questionIndex));
                    int answersArrayLength = questAnswers.length;//only do once instead of every iteration
                    //get the ViewGroup using the viewId based on the questionIndex
                    ViewGroup checkBoxes = findViewById(getGroupViewId(questionIndex));
                    //check that the number of choices in the resource file equals number of checkboxes in the layout
                    if (checkBoxes.getChildCount() != answersArrayLength) {
                        Log.e("INVALID_RESOURCES", "Check the size of \"question_" +
                                String.valueOf(questionIndex) + "_answers\" in strings.xml and " +
                                "the number of choices in q" + String.valueOf(questionIndex) +
                                "_choices in activity_main.xml");
                    }
                    //check that all boxes have the correct boolean state
                    //trim leading and trailing whitespace with trim()
                    for (int ansIndex = 0; ansIndex < answersArrayLength; ansIndex++) {
                        choiceIsChecked = ((CheckBox) checkBoxes.getChildAt(ansIndex)).isChecked();
                        ansIsCorrect &= choiceIsChecked == Boolean.parseBoolean(questAnswers[ansIndex].trim());
                        //look for at least one checked box to determine question was not skipped
                        if (!questionWasAnswered) {
                            questionWasAnswered = choiceIsChecked;
                        }
                    }
                    if (!questionWasAnswered) {
                        score += updateResults(questionResults, AnswerStatus.unanswered, questionIndex);
                    } else if (ansIsCorrect) {
                        score += updateResults(questionResults, AnswerStatus.correct, questionIndex);
                    } else {
                        score += updateResults(questionResults, AnswerStatus.incorrect, questionIndex);
                    }
                    break;
                case text:
                    //get resource value using questionIndex and trim leading and trailing whitespace
                    correctAnswer = QUIZ_RESOURCES.getString(getSingleAnswerId(questionIndex)).trim();
                    //get the TextView using the viewId based on the questionIndex
                    EditText textView = findViewById(getEditTextViewId(questionIndex));
                    //get the user answer text and increase score if it matches the correct answer
                    //trim leading and trailing whitespace with trim()
                    String userAnswerText = textView.getText().toString().trim();
                    if (userAnswerText.isEmpty()) {
                        score += updateResults(questionResults, AnswerStatus.unanswered, questionIndex);
                    } else if (userAnswerText.equalsIgnoreCase(correctAnswer)) {
                        score += updateResults(questionResults, AnswerStatus.correct, questionIndex);
                    } else {
                        score += updateResults(questionResults, AnswerStatus.incorrect, questionIndex);
                    }
                    break;
                default:
                    Log.e("INVALID_QUESTION_TYPE", "Check \"question_" +
                            String.valueOf(questionIndex) + "_type\" in strings.xml");
            }
        }
        //present user with the score and status of questions
        //Insert the score above the questions status. Insert header at beginning of questionResults
        displayUserMessage(questionResults.insert(0, QUIZ_RESOURCES.getString
                (R.string.msg_display_score, score, NUM_QUESTIONS))
                .insert(0, getString(R.string.score_header)).toString());
    }

    /**
     * This method updates the results string with question status and determines if score is to
     * increase
     *
     * @param results reference to StringBuilder object which contains appended results
     * @param status  enum of the status of the question
     * @param qIndex  index of the current question being evaluated
     * @return a 0 if answer status is unanswered or incorrect, a 1 if answer status is correct
     */
    private int updateResults(StringBuilder results, AnswerStatus status, int qIndex) {
        int addToScore = 0;
        switch (status) {
            case unanswered:
                results.append(QUIZ_RESOURCES.getString(R.string.msg_unanswered_ques, qIndex));
                break;
            case correct:
                addToScore = 1;
                results.append(QUIZ_RESOURCES.getString(R.string.msg_correct_ans, qIndex));
                break;
            case incorrect:
                results.append(QUIZ_RESOURCES.getString(R.string.msg_incorrect_ans, qIndex));
        }
        return addToScore;
    }

    /**
     * This method uses a Toast to display a message to the user
     *
     * @param formattedMessage a resource string that can include formatted text
     */
    private void displayUserMessage(String formattedMessage) {
        Context appContext = getApplicationContext();
        Toast toast = Toast.makeText(appContext, formattedMessage, TOAST_DURATION);
        toast.setGravity(Gravity.CENTER, 0, 0);//center message in screen
        toast.show();
    }

    /**
     * This method returns an resource id for question's answer string in strings.xml
     * that can be used to get the value of the string using getString(resId)
     *
     * @param questionNumber the 1 based index of the question being referenced
     * @return the integer resource id for the answer
     */
    private int getSingleAnswerId(int questionNumber) {
        //get the resource id using question_#_answer.
        //force to use english since this is hardcoded in the strings.xml resource file
        return QUIZ_RESOURCES.getIdentifier(String.format(Locale.US,
                "question_%d_answer", questionNumber), "string", PACKAGE_NAME);
    }

    /**
     * This method returns an resource id for a question's answers string array in strings.xml
     * that can be used to get the values of the array using getStringArray(resId)
     *
     * @param questionNumber the 1 based index of the question being referenced
     * @return the integer resource id for the answers array
     */
    private int getQuestionAnswersId(int questionNumber) {
        //get the resource id using question_#_answers
        //force to use english since this is hardcoded in the strings.xml resource file
        return QUIZ_RESOURCES.getIdentifier(String.format(Locale.US,
                "question_%d_answers", questionNumber), "array", PACKAGE_NAME);
    }

    /**
     * This method returns an resource id for a ViewGroup that can be used to
     * get a reference to the view using findViewById
     *
     * @param questionNumber the 1 based index of the question being referenced
     * @return the integer resource id for the ViewGroup
     */
    private int getGroupViewId(int questionNumber) {
        //get the view id using q#_choices. force to use english since this is hardcoded
        //in the activity_main.xml layout file
        return QUIZ_RESOURCES.getIdentifier(String.format(Locale.US,
                "q%d_choices", questionNumber), "id", PACKAGE_NAME);
    }

    /**
     * This method returns an resource id for a EditText view that can be used to
     * get a reference to the view using findViewById
     *
     * @param questionNumber the 1 based index of the question being referenced
     * @return the integer resource id for the view
     */
    private int getEditTextViewId(int questionNumber) {
        //get the view id using question_#_blank
        //force to use english since this is hardcoded in the activity_main.xml layout file
        return QUIZ_RESOURCES.getIdentifier(String.format(Locale.US,
                "question_%d_blank", questionNumber), "id", PACKAGE_NAME);
    }
}
